---
layout: page
title: About
permalink: /about/
---

Xenia Linux is a Gentoo-based distribution which features an immutable root. You can learn more [here](https://xenialinux.com/).

This blog will feature posts about development (as well as the fun stories that happen through development), as well as detailing new releases.
