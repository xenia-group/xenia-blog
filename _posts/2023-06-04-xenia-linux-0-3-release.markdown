---
layout: post
title:  "Xenia Linux 0.3 'Long-haired Chihuahua' is now out!"
date:   2023-06-04 18:20:45 +0100
categories: release
---

| ![](/assets/xenia-linux-0-3-release/desktop.png) |
|:--:|
| *A picture of the Xenia Linux desktop* |

Hi! A couple days ago we released Xenia Linux 0.3 'Long-Haired Chihuahua'! Here is the TL;DR of what we added:

- zram support which gives Xenia Linux swap (finally!)
- Updates to software including GNOME 44.1 and kernel 6.1.27
- Improvements to the base set of software including filesystem tools and partitioning tools (GParted, GNOME Disks)
- An easy upgrade process

It has been a while since we've updated Xenia Linux, but hopefully this post and the addition of a blog (hi!) will make up for that!

## So.. What's with the name?

Xenia Linux was initially supposed to be named after different species of fox - you can see this with 0.1 being named "Zerda" after a fennec fox. With 0.2, we really wanted to highlight Distrobox (and how Xenia could support multiple OSes in containers), so we settled with the name Cerberus.

With 0.3, we didn't have a name until the day before release. So, I looked up "fox breeds" on Google. You can imagine my suprise when different breeds of dogs showed up. So, we picked one of the funnier names (or more like I did - sorry Jack) and went with it.

## Features

### zram support

If you've ever used Fedora before, you might know what zram is - but for those who don't, it makes a compressed area in RAM for swap. That's right - it makes your system move your RAM to RAM when it runs out of RAM!

It's a bit more complex than that - and works *very* well on RAM-limited systems. I tested on my Chromebook with 2GB of RAM and it didn't crash when I opened Firefox! Now, don't think this will make 2G of RAM a *good* experience, but it does work.

| ![](/assets/xenia-linux-0-3-release/chrbook.jpg) |
|:--:|
| *A picture of a wild Chromebook running Xenia* |

A fun little story about this Chromebook, I actually managed to brick the Xenia install. I set the `roots` LV to be 2G, and tried to upload a new root by deleting the existing one and putting the new one it. Unfortunately, Xenia doesn't actually delete the root (it's still mounted!), so I ran out of space. Once I rebooted, the root image was gone. Don't do what I did!

For those wanting to know the details, we implement zram using the OpenRC local service (you can think of this as small scripts that start on boot), and the amount of zram will match the RAM in your system. We implemented zram this way mainly because it is the way Fedora does it, and we think that is a good implementation that works well.

### Base software set improvements

| ![](/assets/xenia-linux-0-3-release/gparted.png) |
|:--:|
| *A picture of Xenia running GParted and GNOME Disks* |

As Xenia is immutable, and has no package layering currently (hint hint), some software is practically impossible to install. Xenia has been kept very minimal from it starting out with no GUI - so we have slowly increased our base software set over time from where we have seen need for certain packages. Xenia now includes:

- A multitude of filesystem tools such as e2fsprogs, dosfstools etc.
- Some packages relating to FUSE
- GParted and GNOME Disks

Xenia is still in very early alpha stages, so we appreciate feedback on this! Feel free to email me or Jack, or open an issue on the [GitLab](https://gitlab.com/xenia-group/catalyst).

We may decide to stop shipping GNOME Disks as it is also packaged as a Flatpak. Let us know what you think about this.

### Easy updates

This is a big one - since the release of Xenia, if the kernel changed between versions, you couldn't update without a convoluted process involving chrooting into the rootfs, extracting the new kernel from the new root, running `grub-mkconfig` again and hoping for the best (it didn't help that we didn't publish this process either). Now, we have changed the logic in GRUB and made the kernel and initramfs have constant names through releases. This means you can do the following to update starting from 0.3:

> Note: this will not work on an existing install of Xenia! This only applies from updates after 0.3.

```bash
mount -o rw,remount /roots # remount /roots to be read-write
cd /roots
rm root.img # or move it somewhere else
wget https://repo.xenialinux.com/releases/current/root.img # or unstable, whichever you prefer
```

### Old features still present

Just as a refresher, or for those new to Xenia, here's what we already have:

- Fully immutable root, image-based
- Flatpak for GUI packages
- Distrobox for CLI packages, or GUI packages not on Flatpak
- Overlays over /var and /etc for system operation
- Having a cool mascot for your OS!

## What's next?

Xenia Linux, as you all are aware by now, is in very early alpha stages! We do now theoretically have an easy upgrade solution, and we have a usable system to the point of being able to daily drive it, but some features we would like are still missing.

### Package layering

We already implement an overlay over /var and /etc, but soon we are looking into making an overlay for /usr that can be activated alongside a helper script to be able to install packages with Portage. This is reliant on the /usr merge which Gentoo does not implement by default, but should soon. We can manually do this with the [merge-usr](https://wiki.gentoo.org/wiki/Merge-usr) script, so we will experiment with this.

We would like for this to be implemented, mainly because it provides users with the option to install packages that aren't packaged in Flatpak and won't work in a Distrobox, for example Tailscale. 

### A nicer onboarding experience - gnome-initial-setup

gnome-initial-setup is something Fedora users will have seen before - it looks like a stripped down GNOME session and sets up a user, your locale, keyboard layout etc. Unfortunately, Gentoo doesn't package this, so we will need to write an ebuild ourselves. 

Once implemented, this will provide you with a much nicer onboarding instead of our current "login with xenia:87658765XeniaLinux and do it yourself".

### Re-do of the installer

To put it bluntly, our installer is pretty terrible. It full on wipes your disk with [no warning](https://www.youtube.com/watch?v=7hiz1BJlWMM), then just tries to download the root image and setup GRUB without doing any checks whatsoever. It does work, but we want to make Xenia a nice experience, and our installer currently doesn't achieve that goal.

We are debating using an installer like Calamares, making our current script nicer, or making something new. Of course, if you have any input, let us know - but currently we are leaning towards making something new. Using an installer like Calamares may be hard as our system isn't a traditional distro, and we do a lot of things differently than most. 

## Conclusion and thank you!

In summary, Xenia Linux now features zram, has a better base set of software and has an easy way to update in the future. Soon, we'll start work on making the package layering system, make a better onboarding experience for users, and make our installer actually usable (lol).

Xenia Linux means a lot to us, and we love seeing people use it as well as seeing feedback. Thank you to all of you that have helped along the way and to those who have tried the distro for themselves!

\- Luna