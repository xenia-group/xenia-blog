---
layout: post
title:  "systemd support, recovery mode, snapshots and more!"
date:   2023-08-13 21:00:00 +0100
categories: preview
---

| ![](/assets/systemd-snapshots-and-more/systemd.png) |
|:--:|
| *A picture of Xenia Linux running systemd* |

It's been a while! So you might be wondering - what have we been up to? First though, there are a couple announcements.

It's with a heavy heart that I announce the departure of one of our developers, Cow. He helped create the installer for Xenia and gave us many ideas to improve the software. Xenia wouldn't be nearly as polished or user-friendly without his work. Not that we don't have a ways to go on that front - but he made Xenia easier to install, which was a *massive* pain point before. 

Secondly, I must welcome the newest member of our team, Mia. Mia has a background in Gentoo Linux and has helped us package all of our software (more on that later!), has helped think through snapshotting with me and is working on a piece of software which will soon bring the `/usr` overlay together for general usage - but I'll let her talk about that in her own post.

Right, let's get on with it.

## systemd support

Xenia Linux has used OpenRC as its init system since we started - and that isn't changing. You might have noticed, however, that documentation has been updated as well as the repos with a systemd version.
Let's get straight to it.
OpenRC, unfortunately, introduces some bugs into Xenia Linux, mainly with dbus and Flatpak. So, out of frustration, I hacked together a systemd version of Xenia for my own use in a couple hours.

It was functional, but things weren't perfect, so I refined the version over a couple days to fix bugs like Distroboxes moving over init systems, and some oddities with the login process.

After those few days of work, I'm proud to announce the existence of the new systemd version! You can freely switch between root images as you please - you can go back and forth between the two by rebooting. systemd brings with it features like timers and user services which may be interesting to those who already use such features.

For now, systemd is in the [unstable branch](https://repo.xenialinux.com/releases/unstable), and documentation reflects this. For now, OpenRC will remain the default - however we are debating whether to switch to systemd by default for 0.5. We will keep building both versions, however!

We would love for you to try out systemd on Xenia! Even if you don't stick with it, there is no harm in trying.

## Recovery mode

| ![](/assets/systemd-snapshots-and-more/recovery.png) |
|:--:|
| *A picture of the Xenia Linux recovery* |

In other OSes, such as macOS and Windows, there is commonly a recovery mode which boots you to a small environment where you can fix things that have gone wrong. Luckily, Xenia is immutable, so problems with the base system are almost impossible - but improper use of the experimental `/usr` overlay can definitely lead to some odd situations!

I implemented a recovery mode by bundling an overlay in to the root image itself - you may notice a directory called `/.recovery` on the new systemd image. The recovery mode is booted in the initramfs, and doesn't mount any of the normal overlays.

Recovery automatically logs you in to a GNOME session. There won't be any of your normal stuff there (Flatpaks, distrobox etc.) but you can use recovery mode to perform anything you want on the overlays - like deleting them, if you want! (please don't)

> Note: This is only implemented in the systemd root at this point (the OpenRC image is slightly outdated).

## Snapshots

With the `/usr` overlay being a bit basic currently, me and our new developer Mia (you're still on the hook for a blog post, remember!) have been working on making the Portage functionality of Xenia more refined.

I won't spoil what she's making, but I'll talk about what I've been doing! I've been working on foxsnapshot, a new tool which manages snapshots over the `/usr` overlay. This allows users to create snapshots easily, and even roll the snapshot back automatically.

Rolling a snapshot in btrfs needs the overlay to be unmounted, so along with the recovery mode, I added rollback support in to foxmount. Foxsnapshot will create a flag, and foxmount will see this, rollback the snapshot and continue booting - not impacting boot times at *all*.

If you would like to try out foxsnapshot early, you can [add the new Xenia portage overlay](https://wiki.xenialinux.com/en/latest/usage/xenia-overlay.html) to your system (more about this in a bit!), and emerge foxsnapshot-9999. You will also need to be using the newest systemd root at this point in time.

## Xenia Portage overlay

As the collection of Xenia software increases, we needed a solution to installing the software into Xenia without hacks. That's when Mia decided to single-handedly make a Portage overlay *and* package our Python software for ebuilds. 

After a little bit of struggle with the Python packaging (seriously, there is like 500 different ways to package Python stuff and none of it is documented well), we now have foxsnapshot and foxcommon (our common libraries shared across programs) packaged.

While this is mainly for use in catalyst, and not for end users to use, but you can add it manually to test newer versions of our software, or for development purposes. You can add it by following the instructions [here](https://wiki.xenialinux.com/en/latest/usage/xenia-overlay.html).

## New documentation

After about a month of making documentation and uploading it to the GitLab wiki, we started reaching the limitations of just that - the GitLab wiki. So, over the last couple of days, I've been migrating the wiki across to [Read the Docs](https://wiki.xenialinux.com/). As we have just migrated, some pages may have broken links - but now there is a way for people out there to help improve the wiki, by submitting a merge request to the Xenia Linux [GitLab](https://gitlab.com/xenia-group/xenia-linux).

Hopefully with these changes we can bring in some new people to Xenia Linux.. and maybe even some contributors!

## Summary

I can say this - Xenia Linux 0.5 is looking like it's going to be a *big* release. With snapshotting and systemd support working now on the latest systemd image (and soon to be OpenRC, please be patient!), I think soon we might be ready for a real, stable release.